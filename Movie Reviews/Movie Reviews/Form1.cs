﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using MetroSet_UI.Forms;
using Movie_Reviews.AppCode;
using Movie_Reviews.Controls;
using Movie_Reviews.Model;
using Sentiment.Analyst;
using Sentiment.Analyst.Models;

namespace Movie_Reviews
{
    public partial class Form1 : MetroSetForm
    {
        private static readonly bool _debugMode = true;

        private SentimentAnalyst _sentimentAnalyst;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var modelDataFile = Path.Combine(GetParentDirectory(),
                _debugMode
                    ? $@"Movie Reviews\\Movie Reviews\\bin\\{"Debug"}\\Data"
                    : $@"Movie Reviews\\Movie Reviews\\bin\\{"Release"}\\Data",
                "model.zip");

            SetColors();

            _sentimentAnalyst = new SentimentAnalyst(null, modelDataFile);
            _sentimentAnalyst.LoadTrainedModel();
        }

        private void MetroSetControlBox1_Click(object sender, EventArgs e)
        {
        }

        private void MetroSetPanel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            if (txtMoviewName.Text == string.Empty)
            {
                MessageBox.Show(this, "Please type a movie name", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMoviewName.Focus();
                return;
            }

            var movieInfo= WebHelper.GetMovieGeneralInfo(txtMoviewName.Text);

            if (movieInfo.Response)
            {
                lblTitle.Text = movieInfo.Title;
                lblYear.Text = movieInfo.Year;
                lblReleased.Text = movieInfo.Released;
                lblRated.Text = $@"{movieInfo.Rated}/10";
                lblRuntime.Text = movieInfo.Runtime;
                lblGenre.Text = movieInfo.Genre;
                lblPlot.Text = movieInfo.Plot;
                lblLanguage.Text = movieInfo.Language;
                lblCountry.Text = movieInfo.Country;
                imdbRating.Text = movieInfo.imdbRating;
                lblInfo11.Text = $@"{movieInfo.imdbRating}/10";
                if (movieInfo.Poster != null)
                    if(movieInfo.Poster!= "N/A")
                        moviePicture.Load(movieInfo.Poster);

                //Get movie reviews
                LoadReviews(WebHelper.GetMovieReviews(movieInfo.ReviewsPageUrl));

            }
            else
            {
                lblTitle.Text = string.Empty;
                lblYear.Text = string.Empty;
                lblReleased.Text = string.Empty;
                lblRated.Text = string.Empty;
                lblRuntime.Text = string.Empty;
                lblGenre.Text = string.Empty;
                lblPlot.Text = string.Empty;
                lblLanguage.Text = string.Empty;
                lblCountry.Text = string.Empty;
                imdbRating.Text = string.Empty;
                moviePicture.Image = null;
                reviewList.Controls.Clear();

                MessageBox.Show(this, "Cannot find the movie, please check the name and try again.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMoviewName.Focus();
            }

        }

        #region Helpers

        //Load user reviews into the list
        private void LoadReviews(IEnumerable<ReviewsModel> reviews)
        {
            var top = 0;

            reviewList.Controls.Clear();

            if(reviews==null) return;
            
            foreach (var review in reviews)
            {
                var data = new Data {Review = review.Review};
                var status = _sentimentAnalyst.Predicate(data);

                var reviewItem = new ReviewItem
                {
                    Width = 485,
                    Top = top,
                    lblTitle = {Text = review.Title},
                    lblDate = {Text = review.Date},
                    lblReview = {Text = review.Review},
                    lblUser = {Text = review.User},
                    lblRank = {Text = $@"{review.Rating}/10"},
                    lblStatus = { Text =  status.PredictionValue==true?"Positive":"Negative" }
                };

                reviewItem.lblStatus.BackColor = status.PredictionValue ? Color.Green : Color.DarkRed;


                reviewList.Controls.Add(reviewItem);
                top += 180;

            }
        }

        //Gets solution path
        private static string GetParentDirectory()
        {
            var directoryInfo = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            if (directoryInfo?.Parent?.Parent != null)
                return directoryInfo.Parent.Parent
                    .FullName;
            return string.Empty;
        }


        //Set colors for the UI elements
        private void SetColors()
        {
            lblInfo1.ForeColor=Color.FromArgb(170,170,170);
            lblInfo2.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo3.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo4.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo5.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo6.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo7.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo8.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo9.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo10.ForeColor = Color.FromArgb(170, 170, 170);
            lblTitle.ForeColor = Color.FromArgb(170, 170, 170);
            lblYear.ForeColor = Color.FromArgb(170, 170, 170);
            lblReleased.ForeColor = Color.FromArgb(170, 170, 170);
            lblRated.ForeColor = Color.FromArgb(170, 170, 170);
            lblRuntime.ForeColor = Color.FromArgb(170, 170, 170);
            lblGenre.ForeColor = Color.FromArgb(170, 170, 170);
            lblLanguage.ForeColor = Color.FromArgb(170, 170, 170);
            lblCountry.ForeColor = Color.FromArgb(170, 170, 170);
            imdbRating.ForeColor = Color.FromArgb(170, 170, 170);
            lblInfo11.ForeColor = Color.FromArgb(170, 170, 170);
            lblPlot.ForeColor = Color.FromArgb(170, 170, 170);
        }

        #endregion Helpers
    }
}