﻿namespace Movie_Reviews.Model
{
    public class ReviewsModel
    {
        public string Title { get; set; }
        public string Review { get; set; }
        public string Rating { get; set; }
        public string User { get; set; }
        public string Date { get; set; }
    }
}