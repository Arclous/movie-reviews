﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using Movie_Reviews.Model;
using Newtonsoft.Json;

namespace Movie_Reviews.AppCode
{
    public static class WebHelper
    {
        //Searches movie in IMDB
        public static MovieSearchResultModel GetMovieGeneralInfo(string movieName)
        {
            var url = string.Format(Settings.API_SEARCH_URL, movieName);

            var request = (HttpWebRequest) WebRequest.Create(url);
            var response = (HttpWebResponse) request.GetResponse();

            var receiveStream = response.GetResponseStream();
            var readStream = new StreamReader(receiveStream ?? throw new InvalidOperationException(), Encoding.UTF8);

            var result =
                JsonConvert.DeserializeObject<MovieSearchResultModel>(readStream.ReadToEnd());

            response.Close();
            readStream.Close();

            if (result.Response)
            {
                result.PageUrl = string.Format(Settings.BASE_MOVIE_URL, result.imdbID);
                result.ReviewsPageUrl = string.Format(Settings.BASE_MOVIE_URL_REVIEWS, result.imdbID);
            }


            return result;
        }

        //Gets user reviews
        public static List<ReviewsModel> GetMovieReviews(string url)
        {
            var web = new HtmlWeb();
            var document = web.Load(url);
            var reviews = GetReviewsNodes(document);

            if (reviews == null) return null;

            return (from data in reviews
                let values = GetUserNameAndData(data)
                select new ReviewsModel
                {
                    Rating = GetRating(data),
                    Title = GetTitle(data),
                    User = values[0],
                    Date = values[1],
                    Review = GetReview(data)
                }).ToList();
        }


        #region HtmlHelpers

        //Gets reviews nodes
        private static IEnumerable<HtmlNode> GetReviewsNodes(HtmlDocument doc)
        {
            return doc.DocumentNode.SelectNodes("//div[contains(@class, 'lister-item-content')]");
        }

        //Gets rating value
        private static string GetRating(HtmlNode node)
        {
            var mainNode = node.SelectSingleNode(".//span[contains(@class, 'rating-other-user-rating')]");
            return mainNode != null ? mainNode.ChildNodes[3].InnerHtml : "N/A";
        }

        //Gets title 
        private static string GetTitle(HtmlNode node)
        {
            return node.SelectSingleNode(".//a[contains(@class, 'title')]").InnerHtml;
        }

        //Gets user and date 
        private static string[] GetUserNameAndData(HtmlNode node)
        {
            var mainNode = node.SelectSingleNode(".//div[contains(@class, 'display-name-date')]");
            var userNode = mainNode.SelectSingleNode(".//span[contains(@class, 'display-name-link')]");
            var user = userNode.SelectSingleNode("a").InnerHtml;
            var date = mainNode.SelectSingleNode(".//span[contains(@class, 'review-date')]").InnerHtml;

            return new[] {user, date};
        }

        //Gets title 
        private static string GetReview(HtmlNode node)
        {
            return node.SelectSingleNode(".//div[contains(@class, 'text show-more__control')]").InnerHtml;
        }

        #endregion
    }
}