﻿namespace Movie_Reviews.AppCode
{
    public static class Settings
    {
        public static string BASE_MOVIE_URL = @"https://www.imdb.com/title/{0}";
        public static string BASE_MOVIE_URL_REVIEWS = @"https://www.imdb.com/title/{0}/reviews?ref_=tt_ov_rt";
        public static string API_SEARCH_URL = @"http://www.omdbapi.com/?t={0}&apikey=33d33b6e";
    }
}