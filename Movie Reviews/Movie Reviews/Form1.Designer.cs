﻿namespace Movie_Reviews
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.styleManager1 = new MetroSet_UI.StyleManager();
            this.metroSetControlBox1 = new MetroSet_UI.Controls.MetroSetControlBox();
            this.txtMoviewName = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetPanel1 = new MetroSet_UI.Controls.MetroSetPanel();
            this.lblInfo1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.btnAnalyze = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetPanel2 = new MetroSet_UI.Controls.MetroSetPanel();
            this.lblInfo2 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetPanel3 = new MetroSet_UI.Controls.MetroSetPanel();
            this.reviewList = new MetroSet_UI.Controls.MetroSetPanel();
            this.metroSetPanel4 = new MetroSet_UI.Controls.MetroSetPanel();
            this.imdbRating = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo10 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblCountry = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo9 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblLanguage = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo8 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblPlot = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblGenre = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo7 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo6 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo5 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo4 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblInfo3 = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblRuntime = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblRated = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblReleased = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblYear = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblTitle = new MetroSet_UI.Controls.MetroSetLabel();
            this.moviePicture = new System.Windows.Forms.PictureBox();
            this.lblInfo11 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetPanel1.SuspendLayout();
            this.metroSetPanel2.SuspendLayout();
            this.metroSetPanel3.SuspendLayout();
            this.metroSetPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moviePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.CustomTheme = "C:\\Users\\Ali\\AppData\\Roaming\\Microsoft\\Windows\\Templates\\ThemeFile.xml";
            this.styleManager1.MetroForm = this;
            this.styleManager1.Style = MetroSet_UI.Design.Style.Custom;
            this.styleManager1.ThemeAuthor = "Narwin";
            this.styleManager1.ThemeName = "Custom";
            // 
            // metroSetControlBox1
            // 
            this.metroSetControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroSetControlBox1.CloseHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.metroSetControlBox1.CloseHoverForeColor = System.Drawing.Color.White;
            this.metroSetControlBox1.CloseNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.DisabledForeColor = System.Drawing.Color.Silver;
            this.metroSetControlBox1.Location = new System.Drawing.Point(1307, 11);
            this.metroSetControlBox1.MaximizeBox = false;
            this.metroSetControlBox1.MaximizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MaximizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MaximizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeBox = true;
            this.metroSetControlBox1.MinimizeHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.metroSetControlBox1.MinimizeHoverForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.MinimizeNormalForeColor = System.Drawing.Color.Gray;
            this.metroSetControlBox1.Name = "metroSetControlBox1";
            this.metroSetControlBox1.Size = new System.Drawing.Size(100, 25);
            this.metroSetControlBox1.Style = MetroSet_UI.Design.Style.Custom;
            this.metroSetControlBox1.StyleManager = this.styleManager1;
            this.metroSetControlBox1.TabIndex = 3;
            this.metroSetControlBox1.Text = "metroSetControlBox1";
            this.metroSetControlBox1.ThemeAuthor = "Narwin";
            this.metroSetControlBox1.ThemeName = "Custom";
            this.metroSetControlBox1.Click += new System.EventHandler(this.MetroSetControlBox1_Click);
            // 
            // txtMoviewName
            // 
            this.txtMoviewName.AutoCompleteCustomSource = null;
            this.txtMoviewName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtMoviewName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtMoviewName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.txtMoviewName.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.txtMoviewName.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.txtMoviewName.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.txtMoviewName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtMoviewName.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.txtMoviewName.Image = null;
            this.txtMoviewName.Lines = null;
            this.txtMoviewName.Location = new System.Drawing.Point(146, 23);
            this.txtMoviewName.MaxLength = 32767;
            this.txtMoviewName.Multiline = false;
            this.txtMoviewName.Name = "txtMoviewName";
            this.txtMoviewName.ReadOnly = false;
            this.txtMoviewName.Size = new System.Drawing.Size(530, 30);
            this.txtMoviewName.Style = MetroSet_UI.Design.Style.Custom;
            this.txtMoviewName.StyleManager = this.styleManager1;
            this.txtMoviewName.TabIndex = 1;
            this.txtMoviewName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMoviewName.ThemeAuthor = "Narwin";
            this.txtMoviewName.ThemeName = "Custom";
            this.txtMoviewName.UseSystemPasswordChar = false;
            this.txtMoviewName.WatermarkText = "Type movie name...";
            // 
            // metroSetPanel1
            // 
            this.metroSetPanel1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.metroSetPanel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.metroSetPanel1.BorderThickness = 0;
            this.metroSetPanel1.Controls.Add(this.lblInfo1);
            this.metroSetPanel1.Controls.Add(this.btnAnalyze);
            this.metroSetPanel1.Controls.Add(this.txtMoviewName);
            this.metroSetPanel1.Location = new System.Drawing.Point(24, 101);
            this.metroSetPanel1.Name = "metroSetPanel1";
            this.metroSetPanel1.Size = new System.Drawing.Size(695, 122);
            this.metroSetPanel1.Style = MetroSet_UI.Design.Style.Custom;
            this.metroSetPanel1.StyleManager = this.styleManager1;
            this.metroSetPanel1.TabIndex = 4;
            this.metroSetPanel1.ThemeAuthor = "Narwin";
            this.metroSetPanel1.ThemeName = "Custom";
            this.metroSetPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.MetroSetPanel1_Paint);
            // 
            // lblInfo1
            // 
            this.lblInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo1.Location = new System.Drawing.Point(12, 23);
            this.lblInfo1.Name = "lblInfo1";
            this.lblInfo1.Size = new System.Drawing.Size(128, 30);
            this.lblInfo1.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo1.StyleManager = this.styleManager1;
            this.lblInfo1.TabIndex = 5;
            this.lblInfo1.Text = "Movie Name:";
            this.lblInfo1.ThemeAuthor = "Narwin";
            this.lblInfo1.ThemeName = "Custom";
            // 
            // btnAnalyze
            // 
            this.btnAnalyze.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnAnalyze.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.btnAnalyze.DisabledForeColor = System.Drawing.Color.Gray;
            this.btnAnalyze.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAnalyze.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.btnAnalyze.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAnalyze.HoverTextColor = System.Drawing.Color.White;
            this.btnAnalyze.Location = new System.Drawing.Point(530, 59);
            this.btnAnalyze.Name = "btnAnalyze";
            this.btnAnalyze.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.btnAnalyze.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.btnAnalyze.NormalTextColor = System.Drawing.Color.White;
            this.btnAnalyze.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.btnAnalyze.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.btnAnalyze.PressTextColor = System.Drawing.Color.White;
            this.btnAnalyze.Size = new System.Drawing.Size(146, 47);
            this.btnAnalyze.Style = MetroSet_UI.Design.Style.Custom;
            this.btnAnalyze.StyleManager = this.styleManager1;
            this.btnAnalyze.TabIndex = 4;
            this.btnAnalyze.Text = "Start Analyze ";
            this.btnAnalyze.ThemeAuthor = "Narwin";
            this.btnAnalyze.ThemeName = "Custom";
            this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
            // 
            // metroSetPanel2
            // 
            this.metroSetPanel2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.metroSetPanel2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.metroSetPanel2.BorderThickness = 0;
            this.metroSetPanel2.Controls.Add(this.lblInfo2);
            this.metroSetPanel2.Controls.Add(this.metroSetPanel3);
            this.metroSetPanel2.Location = new System.Drawing.Point(24, 227);
            this.metroSetPanel2.Name = "metroSetPanel2";
            this.metroSetPanel2.Size = new System.Drawing.Size(893, 435);
            this.metroSetPanel2.Style = MetroSet_UI.Design.Style.Custom;
            this.metroSetPanel2.StyleManager = this.styleManager1;
            this.metroSetPanel2.TabIndex = 5;
            this.metroSetPanel2.ThemeAuthor = "Narwin";
            this.metroSetPanel2.ThemeName = "Custom";
            // 
            // lblInfo2
            // 
            this.lblInfo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo2.Location = new System.Drawing.Point(12, 14);
            this.lblInfo2.Name = "lblInfo2";
            this.lblInfo2.Size = new System.Drawing.Size(168, 23);
            this.lblInfo2.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo2.StyleManager = this.styleManager1;
            this.lblInfo2.TabIndex = 8;
            this.lblInfo2.Text = "User Reviews";
            this.lblInfo2.ThemeAuthor = "Narwin";
            this.lblInfo2.ThemeName = "Custom";
            // 
            // metroSetPanel3
            // 
            this.metroSetPanel3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.metroSetPanel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.metroSetPanel3.BorderThickness = 0;
            this.metroSetPanel3.Controls.Add(this.reviewList);
            this.metroSetPanel3.Location = new System.Drawing.Point(12, 55);
            this.metroSetPanel3.Margin = new System.Windows.Forms.Padding(2003);
            this.metroSetPanel3.Name = "metroSetPanel3";
            this.metroSetPanel3.Size = new System.Drawing.Size(866, 369);
            this.metroSetPanel3.Style = MetroSet_UI.Design.Style.Custom;
            this.metroSetPanel3.StyleManager = this.styleManager1;
            this.metroSetPanel3.TabIndex = 7;
            this.metroSetPanel3.ThemeAuthor = "Narwin";
            this.metroSetPanel3.ThemeName = "Custom";
            // 
            // reviewList
            // 
            this.reviewList.AutoScroll = true;
            this.reviewList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.reviewList.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.reviewList.BorderThickness = 0;
            this.reviewList.Location = new System.Drawing.Point(12, 8);
            this.reviewList.Margin = new System.Windows.Forms.Padding(2003);
            this.reviewList.Name = "reviewList";
            this.reviewList.Size = new System.Drawing.Size(841, 348);
            this.reviewList.Style = MetroSet_UI.Design.Style.Custom;
            this.reviewList.StyleManager = this.styleManager1;
            this.reviewList.TabIndex = 8;
            this.reviewList.ThemeAuthor = "Narwin";
            this.reviewList.ThemeName = "Custom";
            // 
            // metroSetPanel4
            // 
            this.metroSetPanel4.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.metroSetPanel4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.metroSetPanel4.BorderThickness = 0;
            this.metroSetPanel4.Controls.Add(this.imdbRating);
            this.metroSetPanel4.Controls.Add(this.lblInfo10);
            this.metroSetPanel4.Controls.Add(this.lblCountry);
            this.metroSetPanel4.Controls.Add(this.lblInfo9);
            this.metroSetPanel4.Controls.Add(this.lblLanguage);
            this.metroSetPanel4.Controls.Add(this.lblInfo8);
            this.metroSetPanel4.Controls.Add(this.lblPlot);
            this.metroSetPanel4.Controls.Add(this.lblGenre);
            this.metroSetPanel4.Controls.Add(this.lblInfo7);
            this.metroSetPanel4.Controls.Add(this.lblInfo6);
            this.metroSetPanel4.Controls.Add(this.lblInfo5);
            this.metroSetPanel4.Controls.Add(this.lblInfo4);
            this.metroSetPanel4.Controls.Add(this.lblInfo3);
            this.metroSetPanel4.Controls.Add(this.lblRuntime);
            this.metroSetPanel4.Controls.Add(this.lblRated);
            this.metroSetPanel4.Controls.Add(this.lblReleased);
            this.metroSetPanel4.Controls.Add(this.lblYear);
            this.metroSetPanel4.Controls.Add(this.lblTitle);
            this.metroSetPanel4.Controls.Add(this.moviePicture);
            this.metroSetPanel4.Location = new System.Drawing.Point(932, 101);
            this.metroSetPanel4.Name = "metroSetPanel4";
            this.metroSetPanel4.Size = new System.Drawing.Size(483, 561);
            this.metroSetPanel4.Style = MetroSet_UI.Design.Style.Custom;
            this.metroSetPanel4.StyleManager = this.styleManager1;
            this.metroSetPanel4.TabIndex = 7;
            this.metroSetPanel4.ThemeAuthor = "Narwin";
            this.metroSetPanel4.ThemeName = "Custom";
            // 
            // imdbRating
            // 
            this.imdbRating.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.imdbRating.Location = new System.Drawing.Point(319, 240);
            this.imdbRating.Name = "imdbRating";
            this.imdbRating.Size = new System.Drawing.Size(146, 21);
            this.imdbRating.Style = MetroSet_UI.Design.Style.Custom;
            this.imdbRating.StyleManager = this.styleManager1;
            this.imdbRating.TabIndex = 25;
            this.imdbRating.ThemeAuthor = "Narwin";
            this.imdbRating.ThemeName = "Custom";
            // 
            // lblInfo10
            // 
            this.lblInfo10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo10.Location = new System.Drawing.Point(236, 238);
            this.lblInfo10.Name = "lblInfo10";
            this.lblInfo10.Size = new System.Drawing.Size(77, 24);
            this.lblInfo10.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo10.StyleManager = this.styleManager1;
            this.lblInfo10.TabIndex = 24;
            this.lblInfo10.Text = "Rating:";
            this.lblInfo10.ThemeAuthor = "Narwin";
            this.lblInfo10.ThemeName = "Custom";
            // 
            // lblCountry
            // 
            this.lblCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblCountry.Location = new System.Drawing.Point(319, 209);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(146, 21);
            this.lblCountry.Style = MetroSet_UI.Design.Style.Custom;
            this.lblCountry.StyleManager = this.styleManager1;
            this.lblCountry.TabIndex = 23;
            this.lblCountry.ThemeAuthor = "Narwin";
            this.lblCountry.ThemeName = "Custom";
            // 
            // lblInfo9
            // 
            this.lblInfo9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo9.Location = new System.Drawing.Point(236, 207);
            this.lblInfo9.Name = "lblInfo9";
            this.lblInfo9.Size = new System.Drawing.Size(77, 24);
            this.lblInfo9.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo9.StyleManager = this.styleManager1;
            this.lblInfo9.TabIndex = 22;
            this.lblInfo9.Text = "Country:";
            this.lblInfo9.ThemeAuthor = "Narwin";
            this.lblInfo9.ThemeName = "Custom";
            // 
            // lblLanguage
            // 
            this.lblLanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblLanguage.Location = new System.Drawing.Point(319, 182);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(146, 21);
            this.lblLanguage.Style = MetroSet_UI.Design.Style.Custom;
            this.lblLanguage.StyleManager = this.styleManager1;
            this.lblLanguage.TabIndex = 21;
            this.lblLanguage.ThemeAuthor = "Narwin";
            this.lblLanguage.ThemeName = "Custom";
            // 
            // lblInfo8
            // 
            this.lblInfo8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo8.Location = new System.Drawing.Point(236, 180);
            this.lblInfo8.Name = "lblInfo8";
            this.lblInfo8.Size = new System.Drawing.Size(77, 24);
            this.lblInfo8.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo8.StyleManager = this.styleManager1;
            this.lblInfo8.TabIndex = 20;
            this.lblInfo8.Text = "Language:";
            this.lblInfo8.ThemeAuthor = "Narwin";
            this.lblInfo8.ThemeName = "Custom";
            // 
            // lblPlot
            // 
            this.lblPlot.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPlot.Location = new System.Drawing.Point(20, 348);
            this.lblPlot.Name = "lblPlot";
            this.lblPlot.Size = new System.Drawing.Size(455, 98);
            this.lblPlot.Style = MetroSet_UI.Design.Style.Custom;
            this.lblPlot.StyleManager = this.styleManager1;
            this.lblPlot.TabIndex = 19;
            this.lblPlot.Text = "Genre:";
            this.lblPlot.ThemeAuthor = "Narwin";
            this.lblPlot.ThemeName = "Custom";
            // 
            // lblGenre
            // 
            this.lblGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGenre.Location = new System.Drawing.Point(319, 155);
            this.lblGenre.Name = "lblGenre";
            this.lblGenre.Size = new System.Drawing.Size(146, 21);
            this.lblGenre.Style = MetroSet_UI.Design.Style.Custom;
            this.lblGenre.StyleManager = this.styleManager1;
            this.lblGenre.TabIndex = 18;
            this.lblGenre.ThemeAuthor = "Narwin";
            this.lblGenre.ThemeName = "Custom";
            // 
            // lblInfo7
            // 
            this.lblInfo7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo7.Location = new System.Drawing.Point(236, 153);
            this.lblInfo7.Name = "lblInfo7";
            this.lblInfo7.Size = new System.Drawing.Size(77, 24);
            this.lblInfo7.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo7.StyleManager = this.styleManager1;
            this.lblInfo7.TabIndex = 17;
            this.lblInfo7.Text = "Genre:";
            this.lblInfo7.ThemeAuthor = "Narwin";
            this.lblInfo7.ThemeName = "Custom";
            // 
            // lblInfo6
            // 
            this.lblInfo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo6.Location = new System.Drawing.Point(236, 126);
            this.lblInfo6.Name = "lblInfo6";
            this.lblInfo6.Size = new System.Drawing.Size(77, 24);
            this.lblInfo6.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo6.StyleManager = this.styleManager1;
            this.lblInfo6.TabIndex = 16;
            this.lblInfo6.Text = "Runtime:";
            this.lblInfo6.ThemeAuthor = "Narwin";
            this.lblInfo6.ThemeName = "Custom";
            // 
            // lblInfo5
            // 
            this.lblInfo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo5.Location = new System.Drawing.Point(236, 99);
            this.lblInfo5.Name = "lblInfo5";
            this.lblInfo5.Size = new System.Drawing.Size(77, 24);
            this.lblInfo5.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo5.StyleManager = this.styleManager1;
            this.lblInfo5.TabIndex = 15;
            this.lblInfo5.Text = "Rated:";
            this.lblInfo5.ThemeAuthor = "Narwin";
            this.lblInfo5.ThemeName = "Custom";
            // 
            // lblInfo4
            // 
            this.lblInfo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo4.Location = new System.Drawing.Point(236, 72);
            this.lblInfo4.Name = "lblInfo4";
            this.lblInfo4.Size = new System.Drawing.Size(77, 24);
            this.lblInfo4.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo4.StyleManager = this.styleManager1;
            this.lblInfo4.TabIndex = 14;
            this.lblInfo4.Text = "Released:";
            this.lblInfo4.ThemeAuthor = "Narwin";
            this.lblInfo4.ThemeName = "Custom";
            // 
            // lblInfo3
            // 
            this.lblInfo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblInfo3.Location = new System.Drawing.Point(236, 45);
            this.lblInfo3.Name = "lblInfo3";
            this.lblInfo3.Size = new System.Drawing.Size(77, 24);
            this.lblInfo3.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo3.StyleManager = this.styleManager1;
            this.lblInfo3.TabIndex = 13;
            this.lblInfo3.Text = "Year:";
            this.lblInfo3.ThemeAuthor = "Narwin";
            this.lblInfo3.ThemeName = "Custom";
            // 
            // lblRuntime
            // 
            this.lblRuntime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblRuntime.Location = new System.Drawing.Point(319, 128);
            this.lblRuntime.Name = "lblRuntime";
            this.lblRuntime.Size = new System.Drawing.Size(146, 21);
            this.lblRuntime.Style = MetroSet_UI.Design.Style.Custom;
            this.lblRuntime.StyleManager = this.styleManager1;
            this.lblRuntime.TabIndex = 12;
            this.lblRuntime.ThemeAuthor = "Narwin";
            this.lblRuntime.ThemeName = "Custom";
            // 
            // lblRated
            // 
            this.lblRated.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblRated.Location = new System.Drawing.Point(319, 101);
            this.lblRated.Name = "lblRated";
            this.lblRated.Size = new System.Drawing.Size(146, 21);
            this.lblRated.Style = MetroSet_UI.Design.Style.Custom;
            this.lblRated.StyleManager = this.styleManager1;
            this.lblRated.TabIndex = 11;
            this.lblRated.ThemeAuthor = "Narwin";
            this.lblRated.ThemeName = "Custom";
            // 
            // lblReleased
            // 
            this.lblReleased.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblReleased.Location = new System.Drawing.Point(319, 74);
            this.lblReleased.Name = "lblReleased";
            this.lblReleased.Size = new System.Drawing.Size(146, 21);
            this.lblReleased.Style = MetroSet_UI.Design.Style.Custom;
            this.lblReleased.StyleManager = this.styleManager1;
            this.lblReleased.TabIndex = 10;
            this.lblReleased.ThemeAuthor = "Narwin";
            this.lblReleased.ThemeName = "Custom";
            // 
            // lblYear
            // 
            this.lblYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblYear.Location = new System.Drawing.Point(319, 47);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(146, 21);
            this.lblYear.Style = MetroSet_UI.Design.Style.Custom;
            this.lblYear.StyleManager = this.styleManager1;
            this.lblYear.TabIndex = 9;
            this.lblYear.ThemeAuthor = "Narwin";
            this.lblYear.ThemeName = "Custom";
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblTitle.Location = new System.Drawing.Point(236, 18);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(230, 16);
            this.lblTitle.Style = MetroSet_UI.Design.Style.Custom;
            this.lblTitle.StyleManager = this.styleManager1;
            this.lblTitle.TabIndex = 8;
            this.lblTitle.Text = "Movie Name:";
            this.lblTitle.ThemeAuthor = "Narwin";
            this.lblTitle.ThemeName = "Custom";
            // 
            // moviePicture
            // 
            this.moviePicture.Image = ((System.Drawing.Image)(resources.GetObject("moviePicture.Image")));
            this.moviePicture.Location = new System.Drawing.Point(20, 18);
            this.moviePicture.Name = "moviePicture";
            this.moviePicture.Size = new System.Drawing.Size(210, 327);
            this.moviePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.moviePicture.TabIndex = 7;
            this.moviePicture.TabStop = false;
            // 
            // lblInfo11
            // 
            this.lblInfo11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInfo11.Image = ((System.Drawing.Image)(resources.GetObject("lblInfo11.Image")));
            this.lblInfo11.Location = new System.Drawing.Point(753, 91);
            this.lblInfo11.Name = "lblInfo11";
            this.lblInfo11.Size = new System.Drawing.Size(149, 132);
            this.lblInfo11.Style = MetroSet_UI.Design.Style.Custom;
            this.lblInfo11.StyleManager = this.styleManager1;
            this.lblInfo11.TabIndex = 9;
            this.lblInfo11.Text = "10/10";
            this.lblInfo11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblInfo11.ThemeAuthor = "Narwin";
            this.lblInfo11.ThemeName = "Custom";
            // 
            // Form1
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.ClientSize = new System.Drawing.Size(1436, 681);
            this.Controls.Add(this.lblInfo11);
            this.Controls.Add(this.metroSetPanel4);
            this.Controls.Add(this.metroSetPanel2);
            this.Controls.Add(this.metroSetPanel1);
            this.Controls.Add(this.metroSetControlBox1);
            this.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.Name = "Form1";
            this.SmallLineColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.SmallLineColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Style = MetroSet_UI.Design.Style.Custom;
            this.StyleManager = this.styleManager1;
            this.Text = "Movie Reviews";
            this.TextColor = System.Drawing.Color.White;
            this.ThemeName = "Custom";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.metroSetPanel1.ResumeLayout(false);
            this.metroSetPanel2.ResumeLayout(false);
            this.metroSetPanel3.ResumeLayout(false);
            this.metroSetPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.moviePicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.StyleManager styleManager1;
        private MetroSet_UI.Controls.MetroSetControlBox metroSetControlBox1;
        private MetroSet_UI.Controls.MetroSetPanel metroSetPanel1;
        private MetroSet_UI.Controls.MetroSetTextBox txtMoviewName;
        private MetroSet_UI.Controls.MetroSetButton btnAnalyze;
        private MetroSet_UI.Controls.MetroSetPanel metroSetPanel2;
        private MetroSet_UI.Controls.MetroSetPanel metroSetPanel3;
        private MetroSet_UI.Controls.MetroSetPanel reviewList;
        private MetroSet_UI.Controls.MetroSetPanel metroSetPanel4;
        private System.Windows.Forms.PictureBox moviePicture;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo1;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo2;
        private MetroSet_UI.Controls.MetroSetLabel lblTitle;
        private MetroSet_UI.Controls.MetroSetLabel lblYear;
        private MetroSet_UI.Controls.MetroSetLabel lblReleased;
        private MetroSet_UI.Controls.MetroSetLabel lblRated;
        private MetroSet_UI.Controls.MetroSetLabel lblRuntime;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo6;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo5;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo4;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo3;
        private MetroSet_UI.Controls.MetroSetLabel lblGenre;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo7;
        private MetroSet_UI.Controls.MetroSetLabel lblLanguage;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo8;
        private MetroSet_UI.Controls.MetroSetLabel lblCountry;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo9;
        private MetroSet_UI.Controls.MetroSetLabel imdbRating;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo10;
        private MetroSet_UI.Controls.MetroSetLabel lblInfo11;
        private MetroSet_UI.Controls.MetroSetLabel lblPlot;
    }
}

