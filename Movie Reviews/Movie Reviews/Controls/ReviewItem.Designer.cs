﻿namespace Movie_Reviews.Controls
{
    partial class ReviewItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroSetPanel1 = new MetroSet_UI.Controls.MetroSetPanel();
            this.lblRank = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblStatus = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblReview = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblDate = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblUser = new MetroSet_UI.Controls.MetroSetLabel();
            this.lblTitle = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroSetPanel1
            // 
            this.metroSetPanel1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(103)))), ((int)(((byte)(124)))));
            this.metroSetPanel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(78)))), ((int)(((byte)(93)))));
            this.metroSetPanel1.BorderThickness = 0;
            this.metroSetPanel1.Controls.Add(this.lblRank);
            this.metroSetPanel1.Controls.Add(this.lblStatus);
            this.metroSetPanel1.Controls.Add(this.lblReview);
            this.metroSetPanel1.Controls.Add(this.lblDate);
            this.metroSetPanel1.Controls.Add(this.lblUser);
            this.metroSetPanel1.Controls.Add(this.lblTitle);
            this.metroSetPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroSetPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroSetPanel1.Name = "metroSetPanel1";
            this.metroSetPanel1.Size = new System.Drawing.Size(456, 179);
            this.metroSetPanel1.Style = MetroSet_UI.Design.Style.Custom;
            this.metroSetPanel1.StyleManager = null;
            this.metroSetPanel1.TabIndex = 5;
            this.metroSetPanel1.ThemeAuthor = "Narwin";
            this.metroSetPanel1.ThemeName = "Custom";
            // 
            // lblRank
            // 
            this.lblRank.AutoSize = true;
            this.lblRank.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRank.Location = new System.Drawing.Point(3, 75);
            this.lblRank.Name = "lblRank";
            this.lblRank.Size = new System.Drawing.Size(22, 16);
            this.lblRank.Style = MetroSet_UI.Design.Style.Dark;
            this.lblRank.StyleManager = null;
            this.lblRank.TabIndex = 22;
            this.lblRank.Text = "10";
            this.lblRank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblRank.ThemeAuthor = "Narwin";
            this.lblRank.ThemeName = "MetroDark";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStatus.Location = new System.Drawing.Point(40, 72);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(110, 22);
            this.lblStatus.Style = MetroSet_UI.Design.Style.Dark;
            this.lblStatus.StyleManager = null;
            this.lblStatus.TabIndex = 21;
            this.lblStatus.Text = "Movie Name:";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.ThemeAuthor = "Narwin";
            this.lblStatus.ThemeName = "MetroDark";
            // 
            // lblReview
            // 
            this.lblReview.AutoEllipsis = true;
            this.lblReview.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblReview.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblReview.Location = new System.Drawing.Point(0, 103);
            this.lblReview.Name = "lblReview";
            this.lblReview.Padding = new System.Windows.Forms.Padding(5);
            this.lblReview.Size = new System.Drawing.Size(456, 76);
            this.lblReview.Style = MetroSet_UI.Design.Style.Dark;
            this.lblReview.StyleManager = null;
            this.lblReview.TabIndex = 20;
            this.lblReview.Text = "Genre:";
            this.lblReview.ThemeAuthor = "Narwin";
            this.lblReview.ThemeName = "MetroDark";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDate.Location = new System.Drawing.Point(343, 36);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(110, 22);
            this.lblDate.Style = MetroSet_UI.Design.Style.Dark;
            this.lblDate.StyleManager = null;
            this.lblDate.TabIndex = 7;
            this.lblDate.Text = "Movie Name:";
            this.lblDate.ThemeAuthor = "Narwin";
            this.lblDate.ThemeName = "MetroDark";
            // 
            // lblUser
            // 
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUser.Location = new System.Drawing.Point(3, 36);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(110, 22);
            this.lblUser.Style = MetroSet_UI.Design.Style.Dark;
            this.lblUser.StyleManager = null;
            this.lblUser.TabIndex = 6;
            this.lblUser.Text = "Movie Name:";
            this.lblUser.ThemeAuthor = "Narwin";
            this.lblUser.ThemeName = "MetroDark";
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(2, 5, 0, 0);
            this.lblTitle.Size = new System.Drawing.Size(456, 30);
            this.lblTitle.Style = MetroSet_UI.Design.Style.Dark;
            this.lblTitle.StyleManager = null;
            this.lblTitle.TabIndex = 5;
            this.lblTitle.Text = "Movie Name:";
            this.lblTitle.ThemeAuthor = "Narwin";
            this.lblTitle.ThemeName = "MetroDark";
            // 
            // ReviewItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroSetPanel1);
            this.Name = "ReviewItem";
            this.Size = new System.Drawing.Size(456, 179);
            this.metroSetPanel1.ResumeLayout(false);
            this.metroSetPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetPanel metroSetPanel1;
        public MetroSet_UI.Controls.MetroSetLabel lblTitle;
        public MetroSet_UI.Controls.MetroSetLabel lblDate;
        public MetroSet_UI.Controls.MetroSetLabel lblUser;
        public MetroSet_UI.Controls.MetroSetLabel lblReview;
        public MetroSet_UI.Controls.MetroSetLabel lblStatus;
        public MetroSet_UI.Controls.MetroSetLabel lblRank;
    }
}
