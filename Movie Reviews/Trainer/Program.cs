﻿using System;
using System.IO;
using System.Linq;
using Sentiment.Analyst;

namespace Trainer
{
    internal class Program
    {
        private static readonly bool _debugMode = true;

        private static void Main(string[] args)
        {
            var trainingDataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", "IMDBDataset.csv");
            var modelDataFile = Path.Combine(GetParentDirectory(),
                _debugMode
                    ? $@"Movie Reviews\\Movie Reviews\\bin\\{"Debug"}\\Data"
                    : $@"Movie Reviews\\Movie Reviews\\bin\\{"Release"}\\Data",
                "model.zip");


            var sentimentAnalyst = new SentimentAnalyst(trainingDataFile, modelDataFile);


            Console.WriteLine("Training");
            Train(sentimentAnalyst);

            //If you want to see how other models perform
            //TrainMultiple(sentimentAnalyst);

            //If you want to validation
            //CrossValidation(sentimentAnalyst);

            Console.WriteLine("Competed");


            Console.ReadLine();
        }

        private static void Train(SentimentAnalyst sentimentAnalyst)
        {
            //Starts trainer
            var trainingResult = sentimentAnalyst.Train();

            //Displays results of the training
            Console.WriteLine("===============================================");
            Console.WriteLine("Accuracy:{0}", trainingResult.Accuracy);
            Console.WriteLine("AreaUnderRocCurve:{0}", trainingResult.AreaUnderRocCurve);
            Console.WriteLine("F1Score:{0}", trainingResult.F1Score);
            Console.WriteLine("===============================================");
        }

        private static void TrainMultiple(SentimentAnalyst sentimentAnalyst)
        {
            Console.WriteLine("Multiple Training");

            //Starts trainer
            var trainingResults = sentimentAnalyst.TrainMultiple();

            //Displays results of the training
            Console.WriteLine(
                "*************************************************************************************************************");
            Console.WriteLine("*       Training Results      ");
            Console.WriteLine(
                "*------------------------------------------------------------------------------------------------------------");
            foreach (var trainingResult in trainingResults.OrderBy(x => x.Accuracy))
            {
                Console.WriteLine($"* Trainer: {trainingResult.Trainer}");
                Console.WriteLine(
                    $"* Accuracy:    {trainingResult.Accuracy:0.###}  - Area Under Roc Curve: ({trainingResult.AreaUnderRocCurve:#.###})  - F1 Score: ({trainingResult.F1Score:#.###})");
            }

            Console.WriteLine(
                "*************************************************************************************************************");
        }

        private static void CrossValidation(SentimentAnalyst sentimentAnalyst)
        {
            Console.WriteLine("Cross Validating");
            //Starts Validating
            var validationResult = sentimentAnalyst.CrossValidate();

            Console.WriteLine(
                "*************************************************************************************************************");
            Console.WriteLine("*       Metrics for Cross Validation     ");
            Console.WriteLine(
                "*------------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"Trainer: {validationResult.Trainer}");
            Console.WriteLine(
                $"*       Average Accuracy:    {validationResult.AccuracyAverage:0.###}  - Standard deviation: ({validationResult.AccuraciesStdDeviation:#.###})  - Confidence Interval 95%: ({validationResult.AccuraciesConfidenceInterval95:#.###})");
            Console.WriteLine(
                "*************************************************************************************************************");
        }

        //Gets solution path
        private static string GetParentDirectory()
        {
            var directoryInfo = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            if (directoryInfo?.Parent?.Parent != null)
                return directoryInfo.Parent.Parent
                    .FullName;
            return string.Empty;
        }
    }
}