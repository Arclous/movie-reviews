﻿using Microsoft.ML.Data;

namespace Sentiment.Analyst.Models
{
    public class Data
    {
        [ColumnName("review")] [LoadColumn(0)] public string Review { get; set; }


        [ColumnName("sentiment")]
        [LoadColumn(1)]
        public bool Sentiment { get; set; }
    }
}