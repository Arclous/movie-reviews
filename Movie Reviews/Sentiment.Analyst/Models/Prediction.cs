﻿using Microsoft.ML.Data;

namespace Sentiment.Analyst.Models
{
    public class Prediction : Data
    {
        [ColumnName("PredictedLabel")]
        public bool PredictionValue { get; set; }

        public float Score { get; set; }
    }
}