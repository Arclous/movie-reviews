﻿namespace Sentiment.Analyst.Models
{
    public class LearningMethodResult
    {
        public string Trainer;
        public double Accuracy;
        public double AreaUnderRocCurve;
        public double F1Score;
    }
}