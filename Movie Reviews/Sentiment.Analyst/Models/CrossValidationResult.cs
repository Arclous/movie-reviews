﻿namespace Sentiment.Analyst.Models
{
    public class CrossValidationResult
    {
        public string Trainer;
        public double AccuracyAverage;
        public double AccuraciesStdDeviation;
        public double AccuraciesConfidenceInterval95;
    }
}