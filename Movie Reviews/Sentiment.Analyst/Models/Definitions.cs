﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sentiment.Analyst.Models
{
    
        public enum Trainers
        {
            LbfgsLogisticRegression,
            SgdCalibrated,
            SdcaLogisticRegression,
            AveragedPerceptron,
            LinearSvm
        }
    
}
